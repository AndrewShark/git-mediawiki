# Git MediaWiki

Make any article in MediaWiki powered website to appear as a git repository.

Currently, there is no "Who Wrote This" tool as MediaWiki extension. Such thing only exist for Wikipedia site. But you can `git clone` the page and use annotations with `git blame`.

## Configure

Create a config file `~/.config/git-mediawiki.ini`. Add the config section for wiki instance of interest.

Example config file:
```ini
[global]
ide = idea

[wiki.archlinux.org]
article_path = /title/$1
script_path = /
```

The `ide` option in global section controls which ide will be used in printed url when the script finishes.  
See the https://github.com/Ashark/jb-url-handler for possible values.

If unsure about the `article_path` or `script_path`, visit the page "Special:Version", in this example, "https://wiki.archlinux.org/title/Special:Version".

## Usage

```
git-mediawiki https://wiki.archlinux.org/title/Pacman/Package_signing
```

## Result

Go to "&#126;/Documents/MediaWiki/". You will see a wiki instance folder there, in this example, "wiki.archlinux.org/".  
There you will see the tree of folders with article path, including self endpoint name, in this example it will be "Pacman/Package_signing/".  
Inside that, there will be a git repository with a file base name as in article, in this example "Package_signing.mw".  
So the final path to file will be: "&#126;/Documents/MediaWiki/wiki.archlinux.org/Pacman/Package_signing/Package_signing.mw"

Now you can open your tool for viewing git history.  
If using IntelliJ Idea for this, open the "&#126;/Documents/MediaWiki/" as a project root. It will automatically recognise all the other git roots.  
